

const clientes = [
  { id:1,
    nombre:'Leonor',
  },

  {
    id:2,
  nombre:'Jacinto'
  },

  {
    id:3,
    nombre:'Waldo'
  }
 
];

const pagos = [
  { id:1,
    pago:1000,
    moneda:'Bs'
  },

  {
    id:2,
    pago: 1800,
    moneda:'Bs' }

  
 
]

//CALLBACK

const getClientes=(id, callback) => {
  const usuario = {
    id,
    nombre:'Leonor'
  };

  setTimeout(() => {
    callback (usuario);    
  }, 3000);
};

getClientes(1,()=>{
  console.log('hola');
});


//PROMESA

const id = 1;
const getCliente = (id) => {
  return new Promise((resolve,reject)=> {
    const cliente = clientes.find(c =>c.id ===id);
    if (cliente){
      resolve(cliente);
    } else{
      reject(`No existe el cliente con el id ${id}`);
    }
  });
};
// console.log(getCliente(3));


 const getPago = (id) => {
   return new Promise((resolve, reject)=> {
     const pago = pagos.find(p => p.id === id);
     if(pago){
       resolve(pago);
     }else {
       reject( `No se encuentra el pago con el id ${id}`)
     }
   });
};

// console.log(getPagos(3));

//PROMESA NORMAL
 getCliente(id)
 .then(cliente => console.log(cliente))
 .catch(error => {
   console.log(error);
 });

 getPagos(id)
 .then(pago => console.log(pago))
 .catch(error => {
   console.log(error);
 })




//PROMESA EN CADENA 
 let nombre;

 getCliente(id)
   .then(cliente => {
     console.log(cliente);
     nombre = cliente.nombre;
     return getPago(id);
   })
   .then(pago => {
     console.log('El cliente', nombre , 'pago un monto de',pago['pago'], pago['moneda']);
    
   })
   .catch(error => {
     console.log(error);
   })


//ASYNC Y AWAIT

const getCliente2 = (id) => {
  return new Promise((resolve, reject) => {
    const cliente = cliente.find(c => c.id === id);
    if (cliente){
      resolve(empleado);
    }else {
      reject(`No existe el cliente con el id ${id}`);
    }
  });

};

const getPago2 = (id) =>{
  return new Promise((resolve,reject) =>{
    const salario = salarios.find(s => s.id === id);
    if(salario){
      resolve(salario);
    }else {
      reject(`No existe el salario con el id ${id}`)
    }
  });
}


